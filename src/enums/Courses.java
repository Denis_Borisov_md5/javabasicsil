package enums;

public enum Courses {
    /*
    Перечисления курсов
    */
    JAVA1("Технология Java Servlets", 16),
    JAVA2("Обзор технологий Java", 8),
    JAVA3("Библиотека JFC/Swing", 16),
    JAVA4("Технология JDBC", 16),
    JAVA5("Технология JAX", 52),
    FRAMEWORK1("Struts Framework", 24),
    FRAMEWORK2("Spring Framework", 48),
    FRAMEWORK3("Библиотеки commons", 44),
    HIBERNATE("Hibernate", 20);

    private String courseName;
    private int courseDuration;

    Courses (String course, int duration){
        this.courseName = course;
        this.courseDuration = duration;
    }

    public String getCourseName(){
        return courseName;
    }
    public int getCourseDuration(){
        return courseDuration;
    }
}
