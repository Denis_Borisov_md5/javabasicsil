package enums;

public enum  Status {
    /*
    Перечисления статусов студента
    */
    CONTINUE("Potential gradued"),
    HANGING("Возможно продолжение обучения"),
    DISMISS("Вероятно отчисление");

    private String statusVal;

    Status(String status){
        this.statusVal = status;
    }

    public String getStatusVal(){
        return statusVal;
    }
}
