package demo;

import enums.Courses;
import models.Cirrculum;
import models.Student;
import service.*;

import java.util.ArrayList;
import java.util.List;

public class DemoService implements IDemoService{
    @Override
    public void execute(){
        /*
        Сервисы
         */
        IDisplayService displayService = new DisplayService();
        IDurationService durationService = new DurationService();
        IProgressService progressService = new ProgressService();
        IStudentService studentService = new StudentService();

        /*
        Вводим данные
        1. Вводим 2 программы обучения
        2. Вводим 2х студентов
         */
        Cirrculum cirrculum1 = new Cirrculum("J2EE Developer", new Courses[]{Courses.JAVA1, Courses.FRAMEWORK1,Courses.FRAMEWORK2,Courses.HIBERNATE});
        Cirrculum cirrculum2 = new Cirrculum("Java Developer ", new Courses[]{Courses.JAVA2, Courses.JAVA3,Courses.JAVA4,Courses.JAVA5,Courses.FRAMEWORK3});

        Student student1 = new Student(1, "Ivan", "Ivanov", cirrculum1, "17.06.2019");
        Student student2 = new Student(2, "Petr", "Ivanov", cirrculum2, "24.06.2019");

        /*
        Вывод информации о студенте1 содержании программы
         */
        displayService.displayAll(student1);
        System.out.println("Общая длительность программы: " + durationService.countTotalCirruculumDuration(student1.getCirrculum()) + " дн");
        System.out.println("Осталось дней обучения: " + (durationService.countTotalCirruculumDuration(student1.getCirrculum()) -
                durationService.calculateTimePast(student1.getStartDate())) + " дн");
        student1.setProgress(progressService.makeMarks(durationService.calculateTimePast(student1.getStartDate())));
        System.out.println(student1.toString());
        System.out.println("Средний бал: " + progressService.calculateAvg(student1.getProgress()));
        student1.setStatus(progressService.makeStatus(student1.getProgress(), (durationService.countTotalCirruculumDuration(student1.getCirrculum()) -
                durationService.calculateTimePast(student1.getStartDate())), durationService.countTotalCirruculumDuration(student1.getCirrculum())));
        System.out.println(student1.getStatus().getStatusVal());
        System.out.println("///////////////////////////////////////////");

        /*
        Вывод информации о студенте1 содержании программы
         */
        displayService.displayAll(student2);
        System.out.println("Общая длительность программы: " + durationService.countTotalCirruculumDuration(student2.getCirrculum()) + " дн");
        System.out.println("Осталось дней обучения: " + (durationService.countTotalCirruculumDuration(student2.getCirrculum()) -
                durationService.calculateTimePast(student2.getStartDate())) + " дн");
        student2.setProgress(progressService.makeMarks(durationService.calculateTimePast(student2.getStartDate())));
        System.out.println(student2.toString());
        System.out.println("Средний бал: " + progressService.calculateAvg(student2.getProgress()));
        student2.setStatus(progressService.makeStatus(student2.getProgress(), (durationService.countTotalCirruculumDuration(student2.getCirrculum()) -
                durationService.calculateTimePast(student2.getStartDate())), durationService.countTotalCirruculumDuration(student2.getCirrculum())));
        System.out.println(student2.getStatus().getStatusVal());
        System.out.println("///////////////////////////////////////////");

         /*
        Формирование массива студентов
         */
        List<Student> allStudents = new ArrayList<>();
        allStudents.add(student1);
        allStudents.add(student2);

        /*
        Присваивание студентам аттрибутов для сортировки и фильтрации
         */
        student1.setAvgMark(progressService.calculateAvg(student1.getProgress()));
        student2.setAvgMark(progressService.calculateAvg(student2.getProgress()));

        studentService.setDaysLeft(student1,durationService.countTotalCirruculumDuration(student1.getCirrculum()), durationService.calculateTimePast(student1.getStartDate()));
        studentService.setDaysLeft(student2,durationService.countTotalCirruculumDuration(student2.getCirrculum()), durationService.calculateTimePast(student2.getStartDate()));

        /*
        Сортировка и фильтрация
         */
        System.out.println("Сортировка по среднему баллу:");
        studentService.sortStudentListByAvgMark(allStudents);
        displayService.displayStudentArray(allStudents);

        System.out.println("Сортировка по оставшемуся времени:");
        studentService.sortStudentListByDaysLeft(allStudents);
        displayService.displayStudentArray(allStudents);

        System.out.println("Фильтрация списка студентов по условию отчисления:");
        displayService.displayStudentArray(studentService.filterStudentListByStatus(allStudents));
    }
}
