package models;

import enums.Status;

import java.util.Arrays;
import java.util.Objects;

public class Student {
    private int id;
    private String name;
    private String surname;
    private Cirrculum cirrculum;
    private String startDate;

    private int[] progress;
    private double avgMark;
    private Status status;
    private int daysLeft;

    public Student(int id, String name, String surname, Cirrculum cirrculum, String startDate) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.cirrculum = cirrculum;
        this.startDate = startDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Cirrculum getCirrculum() {
        return cirrculum;
    }

    public void setCirrculum(Cirrculum cirrculum) {
        this.cirrculum = cirrculum;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int[] getProgress() {
        return progress;
    }

    public void setProgress(int[] progress) {
        this.progress = progress;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public double getAvgMark() {
        return avgMark;
    }

    public void setAvgMark(double avgMark) {
        this.avgMark = avgMark;
    }

    public int getDaysLeft() {
        return daysLeft;
    }

    public void setDaysLeft(int daysLeft) {
        this.daysLeft = daysLeft;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return id == student.id &&
                name.equals(student.name) &&
                surname.equals(student.surname) &&
                Objects.equals(cirrculum, student.cirrculum) &&
                Objects.equals(startDate, student.startDate) &&
                Arrays.equals(progress, student.progress) &&
                status == student.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, cirrculum, startDate, progress, status);
    }

    @Override
    public String toString() {
        return "MARKS" + Arrays.toString(progress);
    }
}
