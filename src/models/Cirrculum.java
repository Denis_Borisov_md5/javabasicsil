package models;

import enums.Courses;

public class Cirrculum {
    private String cirrculumName;
    private Courses[] courses;

    public Cirrculum(String cirrculumName, Courses[] courses) {
        this.cirrculumName = cirrculumName;
        this.courses = courses;
    }

    public String getCirrculumName(){
        return cirrculumName;
    }

    public Courses[] getCourses(){
        return courses;
    }
}
