package service;

import enums.Status;
import models.Student;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class StudentService implements IStudentService{
    @Override
    public List<Student> sortStudentListByAvgMark(List<Student> allStudents){
        allStudents.sort(Comparator.comparing(Student::getAvgMark));
        return allStudents;
    }

    @Override
    public void setDaysLeft(Student student, int totalCirruculumDuration, int daysPast){
        student.setDaysLeft(totalCirruculumDuration - daysPast);
    }

    @Override
    public List<Student> sortStudentListByDaysLeft(List<Student> allStudents){
        allStudents.sort(Comparator.comparing(Student::getDaysLeft));
        return allStudents;
    }

    @Override
    public List<Student> filterStudentListByStatus(List<Student> allStudents){
        List<Student> filteredStudentList = new ArrayList<>();

        for(Student student : allStudents){
            if(student.getStatus() == Status.DISMISS){
                filteredStudentList.add(student);
            }
        }

        return filteredStudentList;
    }
}
