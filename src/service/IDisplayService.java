package service;

import models.Student;

import java.util.List;

public interface IDisplayService {
   void displayAll(Student student);
   void displayStudentArray(List<Student> allStudents);
}
