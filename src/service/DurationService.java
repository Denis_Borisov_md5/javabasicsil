package service;

import enums.Courses;
import models.Cirrculum;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class DurationService implements IDurationService{
    /*
    Math.ceil - окруляем в бОльшую сторону, чтобы не пропустить день
     */
    @Override
    public int countTotalCirruculumDuration(Cirrculum cirrculum){
        double totalDuration = 0;
        Courses[] courses = cirrculum.getCourses();
        for(Courses course : courses){
            totalDuration += course.getCourseDuration();
        }
        return (int)Math.ceil(totalDuration/8);
    }

    @Override
    public int calculateTimePast(String startDate){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        Date dateStart = null;
        Date dateCurrent = new Date();

        try {
            dateStart = dateFormat.parse(startDate);
        }catch (Exception e){
            System.out.println("Start date format are wrong - " + e);
        }

        return (int)(dateCurrent.getTime() - Objects.requireNonNull(dateStart).getTime())/(24*60*60*1000);
    }
}
