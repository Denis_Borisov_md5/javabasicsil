package service;

import enums.Status;

import java.util.Random;

public class ProgressService implements IProgressService{
    @Override
    public int[] makeMarks(int daysPast){
        int[] allMarks = new int[daysPast];
        Random rand = new Random();

        for(int i = 0; i < daysPast; i++){
            allMarks[i] = rand.nextInt((5 - 1) + 1) + 1;
        }

        return allMarks;
    }

    @Override
    public double calculateAvg(int[] allMarks){
        double avgMark = 0;
        int count = 0;
        for(int mark : allMarks){
            avgMark += mark;
            count++;
        }
        avgMark = Math.rint(avgMark/count*10)/10;

        return avgMark;
    }

    @Override
    public Status makeStatus(int[] allMarks, int daysLeft, int totalCirruculumDuration){
        int sumMarks = 0;
        for(int mark : allMarks){
            sumMarks += mark;
        }
        double passMark = (double)(sumMarks + daysLeft*5)/totalCirruculumDuration;
        if(passMark >= 4.5){
            return Status.HANGING;
        }
        else {
            return Status.DISMISS;
        }
    }
}
