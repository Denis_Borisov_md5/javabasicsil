package service;

import models.Cirrculum;

public interface IDurationService {
    int countTotalCirruculumDuration(Cirrculum cirrculum);
    int calculateTimePast(String startDate);
}
