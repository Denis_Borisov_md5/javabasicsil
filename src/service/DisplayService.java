package service;

import enums.Courses;
import models.Student;

import java.util.List;

public class DisplayService implements IDisplayService{
    @Override
    public void displayAll(Student student){
        System.out.println("STUDENT: " + student.getName() + " " + student.getSurname());
        System.out.println("CIRRCULUM: " + student.getCirrculum().getCirrculumName());
        System.out.println("START_DATE: " + student.getStartDate());
        int count = 1;
        Courses[] allCoursesOfCirrculum = student.getCirrculum().getCourses();
        System.out.println("COURSE / DURATION");
        System.out.println("---------------------------------");
        for(Courses courses : allCoursesOfCirrculum){
            System.out.println(count + ". " + courses.getCourseName() + " / " + courses.getCourseDuration());
            count++;
        }
        System.out.println("---------------------------------");
    }

    @Override
    public void displayStudentArray(List<Student> allStudents){
        for(Student student : allStudents){
            System.out.println(student.getName() + " " + student.getSurname() + " Ср. б.: " + student.getAvgMark() + " Осталось дней: " + student.getDaysLeft() + " дн");
        }
    }
}
