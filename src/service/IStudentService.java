package service;

import models.Student;

import java.util.List;

public interface IStudentService {
    List<Student> sortStudentListByAvgMark(List<Student> allStudents);
    void setDaysLeft(Student student, int totalCirruculumDuration, int daysPast);
    List<Student> sortStudentListByDaysLeft(List<Student> allStudents);
    List<Student> filterStudentListByStatus(List<Student> allStudents);
}
