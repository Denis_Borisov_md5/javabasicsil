package service;

import enums.Status;

public interface IProgressService {
    int[] makeMarks(int daysPast);
    double calculateAvg(int[] allMarks);
    Status makeStatus (int[] allMarks, int daysLeft, int totalCirruculumDuration);
}
